package view;


import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class TelaMenu {

	/**
	 * @wbp.parser.entryPoint
	 */
	public void menu() {

		JFrame janela = new JFrame("Janela");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setSize(600, 300);

		Button button = new Button("VENDENDOR");

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				janela.setVisible(false);
				
			}
		});
		janela.getContentPane().add(button, BorderLayout.EAST);

		Button button_1 = new Button("CLIENTE");

		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				janela.setVisible(false);
				TelaCliente cliente = new TelaCliente();
				cliente.telas();
			}
		});

		janela.getContentPane().add(button_1, BorderLayout.CENTER);

		Button button_2 = new Button("PRODUTO");
		button_2.setForeground(Color.BLACK);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				janela.setVisible(false);

			}
		});
		janela.getContentPane().add(button_2, BorderLayout.WEST);

		janela.setVisible(true);

	}

}
