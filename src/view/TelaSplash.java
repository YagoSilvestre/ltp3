package view;

import javax.swing.JFrame; //imports JFrame library
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton; //imports JButton library

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout; //imports GridLayout libraryimport java.awt.Toolkit;
import java.awt.Toolkit;
 
public class TelaSplash extends JWindow{
 
        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		JFrame frame=new JFrame(); //creates frame

        
        public void showSplash() {
    		JPanel content = (JPanel) getContentPane();
    		content.setBackground(Color.white);

    		// Configura a posi��o e o tamanho da janela
    		int width = 540;
    		int height = 459;
    		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
    		int x = (screen.width - width) / 2;
    		int y = (screen.height - height) / 2;
    		setBounds(x, y, width, height);

    		// Constr�i o splash screen
    		JLabel label = new JLabel(new ImageIcon("splash.jpg"));
    		JLabel copyrt = new JLabel("Copyright 2017, Universidade FUMEC", JLabel.CENTER);
    		copyrt.setFont(new Font("Sans-Serif", Font.BOLD, 12));
    		content.add(label, BorderLayout.CENTER);
    		content.add(copyrt, BorderLayout.SOUTH);
    		Color cor = new Color(0, 0, 125, 255);
    		content.setBorder(BorderFactory.createLineBorder(cor, 5));

    		// Torna vis�vel
    		setVisible(true);

    		// Espera ate que os recursos estejam carregados
    		try {
    			Thread.sleep(1000);
    		} catch (Exception e) {
    		}
    		setVisible(false);
    		this.dispose();
    		return ;
    	}
        public void splesh() {
    		showSplash();
    	}
  
}
