package view;

import java.util.Date;
import java.util.Scanner;

import controller.Comercial;
import erros.SisVendasException;
import model.Cliente;
import util.Auxiliares;

public class RegraGeral {

	private Comercial dados = new Comercial();
	private Scanner entrada = new Scanner(System.in);

	public void cadCliente() {
		
		System.out.println("Digite o nome: ");
		String nome = null;
		nome = entrada.nextLine();
		System.out.println("Digite o telefone: ");
		String telefone = null;
		telefone = entrada.nextLine();
		System.out.println("Digite o email: ");
		String email = null;
		email = entrada.nextLine();
		System.out.println("Digite o cpf: ");
		String cpf = null;
		cpf = entrada.nextLine();
		System.out.println("Digite o limite de credito: ");
		double limiteCredito = 0;
		Auxiliares.aux = entrada.nextLine();

		limiteCredito = Double.parseDouble(Auxiliares.aux);

		System.out.println("teste");

		Date dataCad = new Date(System.currentTimeMillis());

		System.out.println(dataCad);

		if (nome.equals(null) || telefone.equals(null) || email.equals(null) || dataCad.equals(null) || cpf.equals(null)
				|| limiteCredito == 0) {
			SisVendasException.erroCadastro();
		}

		Cliente clientes = new Cliente(nome, telefone, email, dataCad, cpf, limiteCredito);

		dados.dadosCliente.incluirCliente(clientes);

	}

	public void excluirCliente() {
		
		
			String cpf = null;
			System.out.println("escreva o cpf ");
			cpf = entrada.nextLine();
			
			
			dados.dadosCliente.excluirCliente(cpf);		
		
	}

}
