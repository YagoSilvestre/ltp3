package view;

import javax.swing.JFrame;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.awt.Button;

public class TelaCliente {

	/**
	 * @wbp.parser.entryPoint
	 */
	public boolean telas() {

		JFrame janela = new JFrame("Janela");
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setSize(600, 300);

		Button button = new Button("tes");

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				janela.setVisible(false);

			}
		});
		janela.getContentPane().add(button, BorderLayout.EAST);

		Button button_1 = new Button("te");

		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				janela.setVisible(false);
				TelaCliente cliente = new TelaCliente();
				janela.setVisible(cliente.telas());
			}
		});

		janela.getContentPane().add(button_1, BorderLayout.CENTER);

		Button button_2 = new Button("tes");
		button_2.setForeground(Color.BLACK);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				janela.setVisible(false);

			}
		});
		janela.getContentPane().add(button_2, BorderLayout.WEST);

		janela.setVisible(true);

		return true;

	}

}
