package controller;

import java.util.HashMap;
import model.Cliente;
import erros.SisVendasException;

public class RegraCliente {

	private HashMap<String, Cliente> cadClientes = new HashMap<String, Cliente>();

	public void incluirCliente(Cliente clientes) {
		cadClientes.put(clientes.getCpf(), clientes);
	}

	public void excluirCliente(String cpf) {

		if (cadClientes.containsKey(cpf)) {
			cadClientes.remove(cpf);
		} else {
			SisVendasException.erroCpf();
		}

	}

	public Cliente buscarPeloCPF(String cpf) throws SisVendasException {
		if (cadClientes.containsKey(cpf)) {
			return cadClientes.get(cpf);
		} else {
			throw new SisVendasException();
		}
	}

	/*
	 * public ArrayList<Cliente> clientesAlfabeticamente(String cpf) throws
	 * SisVendasException { if (cadClientes.containsValue(cpf)) { return
	 * (ArrayList<Cliente>) cadClientes.values(); } else { throw new
	 * SisVendasException(); } }
	 */

}
