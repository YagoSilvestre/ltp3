package model;
import java.util.GregorianCalendar;

public class Fornecedor extends Pessoa{

	public Fornecedor(String nome, String telefone, String email, int codigo, GregorianCalendar dataCad) {
		super(nome, telefone, email, codigo, dataCad);
		// TODO Auto-generated constructor stub
	}

	private String CNPJ;

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getCNPJ() {
		return CNPJ;
	}

	public void setCNPJ(String cNPJ) {
		CNPJ = cNPJ;
	}

	@Override
	public String toString() {
		return "Fornecedor [CNPJ=" + CNPJ + ", CLIENTE=" + CLIENTE + ", VENDENDOR=" + VENDENDOR + ", FORNECEDOR="
				+ FORNECEDOR + ", nome=" + nome + ", telefone=" + telefone + ", email=" + email + ", codigo=" + codigo
				+ ", dataCad=" + dataCad + ", getCNPJ()=" + getCNPJ() + ", getNome()=" + getNome() + ", getTelefone()="
				+ getTelefone() + ", getEmail()=" + getEmail() + ", getCodigo()=" + getCodigo() + ", getDataCad()="
				+ getDataCad() + ", getCLIENTE()=" + getCLIENTE() + ", getVENDENDOR()=" + getVENDENDOR()
				+ ", getFORNECEDOR()=" + getFORNECEDOR() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	

}
