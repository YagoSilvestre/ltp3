package model;

import java.util.Date;

public class Cliente extends Pessoa {

	@Override
	String tipoPessoa() {
		// TODO Auto-generated method stub
		return null;
	}

	private String cpf;
	private double limiteCredito;
	
	public Cliente(String nome, String telefone, String eamil, Date dataCad, String cpf,
			double limiteCredito) {
		super(nome, telefone, eamil, dataCad);
		this.cpf = cpf;
		this.limiteCredito = limiteCredito;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public double getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	@Override
	public String toString() {
		return "Cliente [cpf=" + cpf + ", limiteCredito=" + limiteCredito + super.toString() +"]";
	}
	
	
	
	
}
