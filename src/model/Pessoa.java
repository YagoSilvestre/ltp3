package model;

import java.util.Date;

 abstract class Pessoa {
	
	
	private String nome;
	private String telefone;
	private String eamil;
	private Date dataCad;
	private int cod;
	private static int seq;
	public Pessoa(String nome, String telefone, String eamil, Date dataCad) {
		super();
		this.nome = nome;
		this.telefone = telefone;
		this.eamil = eamil;
		this.dataCad = dataCad;
		this.cod = seq++;
	}
	
	abstract String tipoPessoa();
	
	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", telefone=" + telefone + ", eamil=" + eamil + ", dataCad=" + dataCad
				+ ", cod=" + cod + "]";
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEamil() {
		return eamil;
	}
	public void setEamil(String eamil) {
		this.eamil = eamil;
	}
	public Date getDataCad() {
		return dataCad;
	}
	public void setDataCad(Date dataCad) {
		this.dataCad = dataCad;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	
	
	
	
}
