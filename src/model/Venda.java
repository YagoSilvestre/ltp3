package model;
import java.util.ArrayList;


public class Venda {

	private Produto produto;
	private	double quantVenda; 
	private	double valorVenda;
	
	private ArrayList <itemVenda> listaCompra = new ArrayList<itemVenda> ();
	
	
		
	
	

	
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public double getQuantVenda() {
		return quantVenda;
	}
	public void setQuantVenda(double quantVenda) {
		this.quantVenda = quantVenda;
	}
	public double getValorVenda() {
		return valorVenda;
	}
	public void setValorVenda(double valorVenda) {
		this.valorVenda = valorVenda;
	}
	public ArrayList <itemVenda> getListaCompra() {
		return listaCompra;
	}
	public void setListaCompra(ArrayList <itemVenda> listaCompra) {
		this.listaCompra = listaCompra;
	}
	@Override
	public String toString() {
		return "Venda [produto=" + produto + ", quantVenda=" + quantVenda + ", valorVenda=" + valorVenda
				+ ", listaCompra=" + listaCompra + ", getProduto()=" + getProduto() + ", getQuantVenda()="
				+ getQuantVenda() + ", getValorVenda()=" + getValorVenda() + ", getListaCompra()=" + getListaCompra()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}



}
