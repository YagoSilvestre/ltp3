package model;
public class itemVenda {
	
	private Produto produto;
	private double quantVenda;
	private double valorVenda;
	public itemVenda(Produto produto, double quantVenda, double valorVenda) {
		this.produto = produto;
		this.quantVenda = quantVenda;
		this.valorVenda = valorVenda;
	}
	
	
	
	
	

	
	
	
	
	
	
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public double getQuantVenda() {
		return quantVenda;
	}
	public void setQuantVenda(double quantVenda) {
		this.quantVenda = quantVenda;
	}
	public double getValorVenda() {
		return valorVenda;
	}
	public void setValorVenda(double valorVenda) {
		this.valorVenda = valorVenda;
	}



	@Override
	public String toString() {
		return "itemVenda [produto=" + produto + ", quantVenda=" + quantVenda + ", valorVenda=" + valorVenda
				+ ", getProduto()=" + getProduto() + ", getQuantVenda()=" + getQuantVenda() + ", getValorVenda()="
				+ getValorVenda() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}
	
		
	
	
	
	
	
}
