package model;
import java.util.GregorianCalendar;

public class Produto {

	
	private int codigo;
	private	String nome;
	private	double precoUnitario;
	private	GregorianCalendar dataCad;
	
	
	
	
	
	
	
	
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public double getPrecoUnitario() {
		return precoUnitario;
	}
	public void setPrecoUnitario(double precoUnitario) {
		this.precoUnitario = precoUnitario;
	}
	public GregorianCalendar getDataCad() {
		return dataCad;
	}
	public void setDataCad(GregorianCalendar dataCad) {
		this.dataCad = dataCad;
	}
	@Override
	public String toString() {
		return "Produto [codigo=" + codigo + ", nome=" + nome + ", precoUnitario=" + precoUnitario + ", dataCad="
				+ dataCad + ", getCodigo()=" + getCodigo() + ", getNome()=" + getNome() + ", getPrecoUnitario()="
				+ getPrecoUnitario() + ", getDataCad()=" + getDataCad() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}
	

	
}
