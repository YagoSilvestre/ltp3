package model;

import java.util.GregorianCalendar;
import java.util.ArrayList;

public class Compra {

	private int numCompra;
	private Fornecedor fornecedor;
	private GregorianCalendar dataCompra;

	private ArrayList<itemCompra> listaCompra = new ArrayList<itemCompra>();

	public int getNumCompra() {
		return numCompra;
	}

	public void setNumCompra(int numCompra) {
		this.numCompra = numCompra;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public GregorianCalendar getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(GregorianCalendar dataCompra) {
		this.dataCompra = dataCompra;
	}

	public ArrayList<itemCompra> getListaCompra() {
		return listaCompra;
	}

	public void setListaCompra(ArrayList<itemCompra> listaCompra) {
		this.listaCompra = listaCompra;
	}

	@Override
	public String toString() {
		return "Compra [numCompra=" + numCompra + ", fornecedor=" + fornecedor + ", dataCompra=" + dataCompra
				+ ", listaCompra=" + listaCompra + ", getNumCompra()=" + getNumCompra() + ", getFornecedor()="
				+ getFornecedor() + ", getDataCompra()=" + getDataCompra() + ", getListaCompra()=" + getListaCompra()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}
